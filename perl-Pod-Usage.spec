%define mod_name Pod-Usage

Name:                perl-%{mod_name}
Epoch:               4
Version:             2.03
Release:             3
Summary:             Print a usage message from embedded pod documentation
License:             GPL-1.0-or-later OR Artistic-1.0-Perl
URL:                 https://metacpan.org/release/%{mod_name}
Source0:             https://cpan.metacpan.org/authors/id/M/MA/MAREKR/%{mod_name}-%{version}.tar.gz


BuildArch:           noarch
BuildRequires:       perl make sed coreutils perl-generators perl-interpreter  perl(Config) perl(warnings)
BuildRequires:       perl(Cwd) perl(ExtUtils::MakeMaker) perl(File::Basename) perl(File::Spec) perl(strict)
BuildRequires:       perl(Carp) perl(Config) perl(Exporter) perl-Pod-Perldoc perl(Pod::Text) perl(vars)
#tests
BuildRequires:       perl(blib) perl(FileHandle) perl(Pod::PlainText) perl(Test::More) 
Requires:            perl(File::Spec) perl-Pod-Perldoc perl(Pod::Text)

%description
Pod2usage will print a usage message for the invoking script (using its embedded pod documentation) 
and then exit the script with the desired exit status. The usage message printed may have any one 
of three levels of "verboseness": If the verbose level is 0, then only a synopsis is printed. If 
the verbose level is 1, then the synopsis is printed along with a description (if present) of the 
command line options and arguments. If the verbose level is 2, then the entire manual page is printed.

%package_help

%prep
%autosetup -n %{mod_name}-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1 NO_PERLLOCAL=1
%{make_build}

%install
%{make_install}
%{_fixperms} $RPM_BUILD_ROOT/*

%check
make test

%files
%doc README Changes
%{_bindir}/pod2usage
%{perl_vendorlib}/*

%files help
%{_mandir}/man1/*
%{_mandir}/man3/*

%changelog
* Sun Jan 19 2025 Funda Wang <fundawang@yeah.net> - 4:2.03-3
- drop useless perl(:MODULE_COMPAT) requirement

* Tue Nov 1  2022 hongjinghao <hongjinghao@huawei.com> - 4:2.03-2
- use %{mod_name} marco

* Tue Oct 25 2022 zhangyao <zhangyao108@huawei.com> - 4:2.03-1
- upgrade version to 2.03

* Wed Jul 28 2021 tianwei <tianwei12@huawei.com> - 4:2.01-2
- output should be empty for issue #I42T4q

* Tue Jan 26 2021 liudabo <liudabo1@huawei.com> - 4:2.01-1
- upgrade version to 2.01

* Sat Jul 25 2020 zhanzhimin <zhanzhimin@huawei.com> - 4:1.70-1
- 1.70 bump

* Thu Sep 12 2019 openEuler Buildteam <buildteam@openeuler.org> - 4:1.69-418
- Package init


